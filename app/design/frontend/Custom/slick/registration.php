<?php
/**
 * @category    Custom
 * @package     Custom\jysk
 * @author      Ricards Grinbergs <info@scandiweb.com>
 * @copyright   Copyright (c) 2019 Scandiweb, Ltd (https://scandiweb.com)
 */

use \Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::THEME, 'frontend/Custom/slick', __DIR__);